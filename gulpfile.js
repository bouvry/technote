var gulp = require('gulp'),
    shell = require('gulp-shell'),
    sass = require('gulp-sass'),
    fs = require('fs'),
    exec = require('child_process').exec;


var src = './src/',
    articles = src + 'articles/';

gulp.task('articles', function () {
    if (!fs.existsSync('dist/articles')) fs.mkdirSync('dist/articles');
    gulp.src(articles + '**/*.md', {read: false})
        .pipe(shell(
            ['pandoc --template=./src/templates/article.html --standalone <%= file.path %> -o <%= f(file.path) %>'],
            {
                templateData: {
                    f: function (s) {
                        return s.replace(/\.md/, '.html').replace(/src\//, 'dist/');
                    }
                }
            }
        ))
});

gulp.task('images', function () {
    gulp.src('src/images/**.*')
        .pipe(gulp.dest('dist/images'))
});

gulp.task('slides', function () {
  if (!fs.existsSync('dist/slides')) fs.mkdirSync('dist/slides');
  gulp.src('./src/slides/*.md', {read: false})
      .pipe(shell(
          ['pandoc --variable revealjs-url="../libs/reveal.js" --template=src/templates/slide.html --standalone --section-divs --variable theme="simple" --variable transition="default" -s -i -t revealjs <%= file.path %> -o <%= f(file.path) %>'],
          {
              templateData: {
                  f: function (s) {
                      return s.replace(/\.md/, '.html').replace(/src\//, 'dist/');
                  }
              }
          }
      ))
  //pandoc -t revealjs -s -o myslides.html myslides.md -V revealjs-url=http://lab.hakim.se/reveal-js
});

gulp.task('index', function () {
    gulp.src('src/*.md', {read: false})
        .pipe(shell(
            ['pandoc --template=./src/templates/index.html --standalone <%= file.path %> -o <%= f(file.path) %>'],
            {
                templateData: {
                    f: function (s) {
                        return s.replace(/\.md/, '.html').replace(/src\//, 'dist/');
                    }
                }
            }
        ))
});


gulp.task('sass', function () {
    gulp.src('src/styles/**/*.scss')
    //.pipe(cached('sass'))
        .pipe(sass())
        .pipe(gulp.dest('dist/styles'));
});


gulp.task('default', ['articles', 'slides', 'index', 'images', 'sass']);

gulp.task('watch', ['default'], function () {
    gulp.watch('src/slides/*.md', ['slides']);
    gulp.watch('src/templates/slide.html', ['slides']);
    gulp.watch('src/index.md', ['index']);
    gulp.watch('src/templates/index.html', ['index']);
    gulp.watch('src/articles/*.md', ['articles']);
    gulp.watch('src/templates/article.html', ['articles']);
    gulp.watch('src/styles/*.scss', ['sass']);
    gulp.watch('src/images/*.*', ['images']);
})
