 # Intégration web

  - Introduction : Les technologies du web

  - HTML5
    * Création de page web
    * Syntaxe et règle
    * Structurer l'information
    * Gestion de site : Lien, images et contenu externe

  - CSS3
   * Intégration CSS
   * Syntaxe, sélecteurs et régles
   * Mise en forme du texte
   * Mise en page 1 : Le modèle de boîtes
   * Mise en forme des boites (bases)   
   * Mise en forme et habillage des boites avancés
   * Mise en page (Flexbox)
   * Mise en page avancé (Positionnement libre)
   * Mise en page avancé (Le responsive design)
