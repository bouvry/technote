% CSS : Modèle de boîte
% Stéphane Bouvry
% 2018

# Principe de base


## Flux courant

Par défaut, le contenu d'une page est traité comme un flux de texte :  **le flux courant**.

Deux type de comportement existent :

 - `block` : une boîte qui occupe toute la largeur disponible (elle rompt le flux), et ajuste sa hauteur en fonction de son contenu
 - `inline` : Une boite qui *s'empile* de gauche à droite sans rompre le flux.


## border

La propriété `border` permet d'ajuster la taille de la **bordure**.

![](../images/boite-border.svg)


## margin

La propriété `margin` permet d'ajuster la taille de la **marge extérieur**.

![](../images/boite-margin.svg)


## padding

La propriété `padding` permet d'ajuster la taille de la **marge intérieur**. Entre le bord de la boîte et le contenu de la boite.

![](../images/boite-padding.svg)

## width et height

Les propriété `height` et `width` permettent d'ajuster la **taille de la boîte**.

![](../images/boite-size.svg)


## Attention : taille des blocs

Par défaut, la largeur effective d'un boîte nécessite d'additionner le `width`, `border` et `padding` et `margin`

la règle suivante permet de régler le soucis :

```css
* {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
```

## prendre de la hauteur

Les éléments `inline` n'effectent pas le flux courant verticalement.


## display

On peut modifier le comportement d'un élément en utilisant la propriété `display` :

```css
h1 {
  background-color: yellow;
  display: inline;
}

strong {
  display: block;
}
```

La valeur `inline-block` permet de forcer la prise en charge de l'empattement vertical au éléments `inline`.

```css
h1 {
  background-color: yellow;
  display: inline;
}

strong {
  display: inline-block;
}
```

## Informations importantes

> - Une marge `margin` peut être négative, dans ce cas, l'élément déborde, il peut même disparaître hors champs.
> - Si une boite est trop petite pour son contenu, le contenu va simplement dépasser de la boîte (sauf contre indication).
> - Les éléments `inline` ignorent les modifications verticales

# FLOAT

## Présentation

la propriétés CSS `float` est ancienne (CSS2).

Elle a donné lieu à un des outils les plus tordue pour faire de la mise en page, et certains l'utilise encore aujourd'hui.

La technique des **boîtes flottantes**

## Usage normal

A l'origine, la propriété `float` a été imaginé pour  permettre à un contenu (texte) de s'écouler autour d'une image.

```html
<img src="image.jpg" alt="">
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
```
```css
img { float: left; }
```

## Effet

Quand un élément *flotte*, plusieurs chose se produisent :

> - L'élément **sort du flux courant**
> - La largeur de l'élément d'adapte à son contenu
> - Le **contenu** des autres boïtes *s'écoulent*
> - Les autres éléments *flottant* s'écoule

DEMO

## CLEARER

```css
.clearer {
    border: none;
    clear: both;
    height: 0;
    margin: 0;
    padding: 0;
}
```

## CLEARFIX

```css
.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}
```
