% Gestion de site
% Stéphane Bouvry
% 2018

# Les URL : Uniform Resource Locator

## Introduction

Dans cette partie, nous aborderons la gestion de site (fichiers), la liaison entre les documents (URL) ainsi que la mise en ligne.

## URL (Uniform Resource Locator)

Les **Localiseur uniforme de ressource**, ou **adresses réticulaires** (traduction québéquoises), ou plus vulgairement **adresse web**, permettent de localiser des ressources : site, image, pages web, fichiers CSS, etc...


On distingue 2 types d'URL : **relative** et **absolue**

## URL Absolue

Une URL absolue est une adresse complète, Ex: http://cours.jacksay.com/fichier.html

Une URL absolue se compose toujours de la même façon : `[protocol]://[serveur]/[chemin vers fichier]`

C'est comme une adresse postale internationale

## URL Relative

Une URL relative correspond à l'emplacement d'une ressource(fichier HTML, CSS, images, etc...) relativement à une autre.

Un chemin relatif parcourt une arborescence, on rencontre différents cas :

 - Fichiers au même niveau,
 - Fichiers plus bas dans des dossiers (dans un dossier enfant),
 - Fichiers plus haut (dans un dossier parents),
 - Fichiers dans un autre dossier,


## URL Relative : même niveau

<div class="cols">
<div class="col3">

Quand les fichiers sont au même niveau(dans le même dossier), l'URL correspond simplement au nom du fichiers.

Dans cet exemple, pour allé de **index.html** vers **annexes.html**, l'URL sera **annexes.html**

</div>
<div class="col">

![Arborescence](../images/chemins-relatifs-01.png)

</div>
</div>


## URL Relative : descendre


<div class="cols">
<div class="col">

![Arborescence 2](../images/chemins-relatifs-02.png)

</div>
<div class="col3">

On peut descendre dans une arborescence :

Dans cet exemple, pour allé de **fichier.html**
vers **image1.jpg**, le chemin parcourt :

`images/photos/archives/image1.jpg`

l'URL est donc **images/photos/archives/image1.jpg**

</div>
</div>


## URL Relative : remonter

<div class="cols">
<div class="col3">


On peut remonter dans une arborescence pour atteindre un dossier parent en utilisant `../`



Dans cet exemple, pour allé de les-liens.html
vers index.html, le chemin doit remonter de 2 dossiers :

l'URL est donc **../../index.html**

</div>
<div class="col">

![Arborescence 2](../images/chemins-relatifs-03.png)

</div>
</div>


## URL Relative : remonter et redescendre


<div class="cols">
<div class="col">

![Arborescence 2](../images/chemins-relatifs-04.png)

</div>
<div class="col3">

On peut cumuler la remontée et la descente avec `../` :

Dans cet exemple, pour allé de fichier.html (dans le dossier documents)
vers image1.jpg, le chemin doit d'abord remonter dans le dossier racine, puis
descendre jusqu'au fichier

../ on remonte dans le dossier racine)

images/photos/archives (on redescend les dossiers)


image1.jpg (... jusqu'au fichier, enfin)



l'URL est donc
../images/photos/archives/image1.jpg

</div>
</div>




## Exercice

Dans l'arborescence ci-contre, indiquer les URL relatives :

<div class="cols">
<div class="col2">

index.html &gt;
9eme-symfony.html


index.html &gt;
beethoven.jpg


index.html &gt;
lv-beethoven.html


lv-beethoven.html &gt;
beethoven.jpg


lv-beethoven.html &gt;
david-lynch.html


lv-beethoven.html &gt;
9eme-symfony.html


lv-beethoven.html &gt;
index.html


david-lynch.html &gt;
eraserhead.html


david-lynch.html &gt;
index.html


david-lynch.html &gt;
david-lynch.jpg


9eme-symfony.html &gt;
lv-beethoven.html

</div>
<div class="col">

![Arborescence 2](../images/arbo.png)

</div>
</div>



# Documents externes

## La balise `A`

La balise A permet de faire des liens. Elle dispose d'un attribut `HREF` permettant de spécifier l'URL à atteindre

```html
Exemple de texte avec
un <a href="http://cours.jacksay.com">lien dedans</a>.
```

Traditionnelement, les liens sont placés sur du texte, mais il est possible d'entourer des structures plus complexe (image, bloc entier).

## Balise A : Attributs facultatifs


|  Attribut    | Description
|--------------|-----------------------------------------------------------------------------
| `title`      | Donne une description du lien (Produit une bulle d'aide).
| `role`       | Cet attribut est normalisé. Il est utilisé dans le cadre des normes d'accessibilité.
| `rel`        | Décrit la relation entre le document courant et le lien. Là aussi, une norme existe pour standardiser la relation.
| `download` | Cet attribut permet de forcer le téléchargement de l'URL de déstination. La valeur correspond au nom du fichier sur le client (pas necessairement le nom du fichier)


## Les images

La balise `img` est une balise *inline*. La source de l'image à afficher est renseignée par l'attribut `src` :

L'attribut `alt` permet d'indiquer un contenu alternatif textuel.

```html
<img src="URL" alt="" />
```


<div class="info">
L'attribut `alt` est obligatoire, mais rien d'interdit de le laisser vide.
</div>


## Figure

Les balises `figure` et `caption` sont utilisées pour ajouter une portée sémantique à l'intégration d'une image.

figure est le conteneur global et la balise figcation permet de délimiter la légende.

```html
<figure>
  <img src="images/illustration.jpg" alt="" />
  <figcaption>Space Writing - Man Ray - 1937</figcaption>
</figure>
```

La balise figure est plus générale, elle peut délimiter plusieurs images, un extrait de code, une vidéo, etc...


## CSS et image

Si une image est destiné à faire de l'habillage, on préfèrera utiliser CSS et la propriété background-image.

```css
.bandeau-site {
    background-image: url('bandeau.jpg');
}
```
L'URL de l'image est relatif à l'emplacement du CSS.


## Les ancres

Les ancres sont utilisées pour naviguer dans une portion de document. Ce sont des URL se terminant par un #ID-DANS-LA-PAGE

```html
<a href="mon-document.html#partie3">Portion 1
```

L'expression qui suit le dièse correspond à un identifiant de balise :

```html
<section id="partie3">
<h1>Part 3 : Les liens</h1>
Dans cette ...
```
Pour naviguer au sein d'un même document, les URL se composent simplement du # et d'un ID à atteindre

```html
<a href="#partie2">Aller à la partie 2
```
Ce mécanisme permet par exemple de faire des tables des matières

## Vidéo

La balise video porte bien son nom.

```html
<source src="video.mp4" type="video/mp4" />
<source src="video.webm" type="video/webm" />
<source src="video.ogv" type="video/ogg" />
Contenu alternatif
```

controls="true" : Affiche les contrôles
autoplay="true" : Lecture automatique
preload="auto" : Chargement en cache auto
poster="image.jpg" : Vignette par défaut
loop="true" : Lecture en boucle


Chaque navigateur a son propre format, voir cet article

[Balise video sur Alsacréation](http://www.alsacreations.com/article/lire/1125-introduction-balise-video-html5-mp4-h264-webm-ogg-theora.html).

## Audio

TODO

## Arborescence d'un site

Un site web est souvent composé de plusieurs fichiers :

Document HTML,
Images,
médias,
documents type PDF, Doc,
Fichiers CSS,
Scripts,
Etc...

Ces fichiers doivent être correctement organisé


## Architecture type

<div class="cols">
<div class="col">
Le choix des noms de fichiers est important car il favorise le
référencements, on dit également qu'il doit être *human readable*

Les noms des fichiers doivent être en minuscules, sans espaces ni caractères accentués.

La page d'accueil doit toujours s'appeller index.html

Au delà de la convention, les serveurs web sont configurer pour
afficher automatiquement cette page

</div>
<div class="cols">

![Arborescence classique](../images/arbo-site.png)

</div>
</div>


## Héberger son site : Espace web

Pour qu'un site soit visible depuis internet, il faut qu'il soit dans un espace web (serveur).

Il s'agit d'un simple dossier sur une machine (une machine serveur).

Sur cette machine est installé un logiciel spécial, un serveur web, et ce serveur est configuré pour rediriger certaines requètes HTTP vers certains espaces web.

Le serveur le plus utiliisé est apache.



<!--


<slide title="Le serveur Web">

<img src="images/illustrations/apache.png" />



<slide title="L'hébergeur">

On peut tout à fait installer son propre serveur web chez soit.
Mais il est plus simple d'avoir recourt à un hébergeur dont c'est la spécialité,
Celui ci vous donnera un identifiant
et un espace web
Et vous aurez accès à votre si via l'IP de la machine qui héberge
votre site grace à une adresse de ce type : http://89.158.14.8/~identifiant/

heu...






Les noms de domaine
<slide title="L'adresse IP">

A l'origine, les machines communiquent sur le réseaux en
utilisant un numéro unique (une sorte de numéro de sécurité social), l'IP
(Internet protocol)

<img src="images/illustrations/network-ip.png" />



<slide title="Nom de domaine">

Nous pourrions utiliser les IP des serveurs pour visualiser certains sites qu'ils hébergent, mais
ça n'est pas très mémo-technique. Alors pour simplifier l'accès aux machines, on c'est dit qu'il
serait bon de leur donner des noms, les noms de domaine...


Le choix du nom de domaine peut favoriser le référencement d'un site. Si par exemple vous faites
un site pour Maurice, un plombier, choisissez un nom de domaine type : plombier-maurice.fr.
La séparation des mots clefs par un tiret est importante.


Les noms de domaine sont (sur internet) suffixé d'une extension (.fr .com., etc...)
Exemple : jacksay.com
auto-promo a peine caché :P

Mais pourquoi on met des double vé devans ?




<slide title="Les sous-domaines">

Un nom de domaine, ça ne sert pas que pour les sites, ça sert également
pour les mails, d'autres services, identifier des machines... Donc pour pouvoir faire plein de chose
avec un même nom de domaine, on les a hiérarchisés avec les sous-domaine...

Exemple :

mail.jacksay.com (pour les mails)
sql.jacksay.com (pour les serveurs SQL)
pouet.jacksay.com (pour les... heu)

Et le World Wide Web c'est donc vu attribuer (par convention),
le sous domaine www.


Sur beaucoup d'hébergeur, le dossier servant d'espace web s'appelle d'ailleurs www






<slide title="Obtenir un domaine">

L'obtention d'un nom de domaine passe par un registar

Il s'agit d'une société privé qui assure la mise en place technique et
administrative d'un nom de domaine. (N'importe qui ne peut pas attribuer des nom de domaine)

Selon le type d'extension, le prix d'un domaine à l'année peut
varier de quelques euros à plusieurs centaines...


Sans que cela soit une necessité, on achète généralement
un nom de domaine et l'espace web en même temps, cela se présente d'ailleurs
sous la forme d'une offre (mutualisée).


Une fois votre domaine réservé et l'espace web ouvert, il arrive que l'accès
ne soit pas instantané, et ce à cause du temps de propagation




<slide title="Propagation DNS">

Même si on utilise des noms de domaine, les machines continuent d'utiliser les IP,
et pour savoir à quelle IP correspond tel domaine, elles se réfère à des serveurs spéciaux :
Les serveurs DNS.

<img src="images/illustrations/network-dns.png" />



<slide title="Résumé">

Pour hébergé un site, il faut donc :

un espace web sur une machine disposant d'un serveur web
un nom de domaine configuré sur les serveurs DNS

Tout ça étant fournis pas l'hébergeur





Mettre en ligne son site
<slide title="Transfert FTP">

Il faut maintenant déposé dans l'espace web
les fichiers de son site.

Plusieurs solutions existent, mais la plus usité est le
transfert FTP (File Transert Protocol)


<img src="images/illustrations/network-ftp.png" />



<slide title="connexion FTP">

Dans le cadre d'une mise en ligne de site, la connexion de votre client FTP au
serveur FTP de l'hébergeur va necessiter plusieurs choses :

Un login (ou identifiant)
Un mot de passe
L' adresse du serveur FTP (souvent, ftp.ledomaine.ext)


Ces informations vous seront fournies par l'hébergeur
​                    -->
​                
​            
