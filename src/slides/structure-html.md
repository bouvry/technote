% Structure HTML
% Stéphane Bouvry
% 2018


# Balises textuelle

## Sémantique

Les balises qui suivent sont affichées avec une certaine mise en forme dans le navigateur, par exemple la balise `<strong>` est affiché en gras. MAIS ce *confort* ne doit pas être retenu lors du choix des balises car l'apparence sera géré par le CSS par la suite.

Donc utiliser ces balises pour la valeur **sémantique** uniquement.

## Bloc

Quelques balises courantes pour structurer des blocs de contenu en HTML

 - `<h1>`,`<h2>`,`<h3>`, `<h4>`, `<h5>`, `<h6>` : Titres
 - `<p>` : Paragraphes
 - `<ul><li>` : Listes
 - `<ol><li>` : Liste ordonée
 - `<blockquote>` : Bloc de citation
 - `<pre>` : Préformatté (un truc de développeur)
 - `<address>` : Pour... les adresses


## Texte

Quelques balises donnant du sens au texte.

 - `<em>` : Emphase
 - `<strong>` : Important
 - `<abbr>` : Abbréviation
 - `<acronym>` : Acronyme
 - `<i>` : Terme technique
 - `<q>` : Citation en ligne
 - `<sub>` : Indice
 - `<sup>` : Exposant
 - `<time>` : Données temporelles
 - `<code>` : Du code
 - `<small>` : Texte de moindre importance

## Autres

 `<figure>` : Encadre une illustration (fonction souvent avec `<img />` et `<figcaption>`)

```html
<figure>
    <img src="guernica.jpg" alt="Guernica">
    <figcaption>Guernica - Pablo Picasso - 1937</figcaption>
</figure>
```

# Balises de structure

## Sémantique toujours

Ces balises sont utilisées pour hiérarchiser les informations et fournir une structure **sémantique**.

## ARTICLE

Délimite un contenu auto-suffisant (pas forcement un article au sens journalistique).

En théorie, le contenu d'une balise article doit pouvoir être réutilisé tel quel.

Exemple : Dans un site d'actualité, une liste d'articles sera une serie de balise article.

```html
<body>
    <h1>Liste des artilces</h1>
    <article>
        <h2>Article 1</h2>
        <p>Texte</p>
    </article>
    <article>
        <h2>Article 1</h2>
        <p>Texte</p>
    </article>
</body>
```

## SECTION

La balise `<section>` permet de regrouper des contenus qui ont un thème communs.

```html
<h1>Liste des artilces</h1>
<section>
    <h2>Peinture</h2>
    <article>
        <h2>Jean-Michel Basquiat</h2>
        <p>Texte</p>
    </article>
    <article>
        <h2>Francis Bacon</h2>
        <p>Texte</p>
    </article>
</section>

<section>
    <h2>Musique</h2>
    ...
</section>
```

## HEADER

Elle délimite un contenu qui va introduire la suite : titre, résumés, métas informations (date de publication, auteur, mots clefs).

```html
<body>
    <header>
        <h1>Titre du site</h1>
        <h2>Accroche</h2>
        <p>prambule</p>
    </header>
    <section>
        <header>
            <h2>Titre</h2>
        </header>
    </section>
</body>
```

## FOOTER

Délimite un contenu qui va conclure : Pied de page

```html
<body>
    <header>
        <h1>Titre du site</h1>
    </header>
    <section>
        <header>
            <h2>Titre</h2>
        </header>
    </section>
    <footer>
        Pied de page
    </footer>
</body>
```

## NAV

La balise `<nav>` permet de délimiter un contenu qui va permettre la navigation.

Cette navigation doit être relative à l'emplacement de la balise (Table des matière d'un article, sommaire d'une section, menu d'un site, etc...).

```html
<body>
    <header>
        <h1>Titre du site</h1>
        <h2>Accroche</h2>
    </header>
    <nav>
        <a href="accueil.html">Actualités</a>
        <a href="peintures.html">Peinture</a>
        <a href="litterature.html">Littérature</a>
    </nav>
</body>
```

## ASIDE

> Il délimite un contenu tanganciellement relatif au contenu auquel il est attaché, sans que le contenu soit dépendant de l'aside pour être compris

On l'utilise généralement sur le site entier (body) ou un  article pour délimiter une information complémentaire. Ex : Citation, glossaire dans un article. Liste de liens dans un site

```html
<article>
    <h1>Titre</h1>
    <p>HTML et CSS</p>
    <aside>
        <h3>Glossaire</h3>
        <dl>
            <dt>CSS</dt>
            <dd>Cascading Style Sheet</dd>
            <dt>HTML</dt>
            <dd>HyperText Markup Language</dd>
        </dl>
    </aside>
</article>
```

# Divers

## Attributs communs

Voici la liste des attributs communs à toutes le balises (et souvent facultatifs) : </p>

| Attribut  | Description
|-----------|-----------------------------------------------------------------
| `class`   | Permet d'assigner une ou plusieurs classes CSS à l'éléments
| `id`      | Fixe l'identifiant (unique) de l'élément.
| `style`   | Définit une "CSS en ligne"
| `title`   | Description complémentaire (affiche une info-bulle)
| `lang`    | Précise la langue utilisée dans le contenu (si différente de celle du document)


## Balises neutres

Ces balises n'ont pas fonction sématique, elles sont (très) utilisées par les intégrateurs pour structurer la page pour la mettre en forme (délimiter des colonnes, des zones)

 - `<div>` : Zone type bloc
 - `<span>` : Contenu texte (dans le flux)
