% Mise en forme CSS
% Stéphane Bouvry
% 2018

# Introduction

## Un monde vaste

Il existe plusieurs 100ènes de propriétés CSS pour gérer la mise en forme des documents.

Nous aborderons ici quelques propriétés usuelles uniquement. Pour plus de détail sur les usages / problème de compatibilité,
le site : [MDN](https://developer.mozilla.org/fr/docs/Web/CSS) centralise une documentation en français exhaustive maintenue par une communauté de développeur (une sorte de wikipedia pour intégrateur Web).

## Préfixe

Les préfixes sont généralement utilisés pour garantir la viabilité d'un propriété sur des navigateurs dattés

ex : [Voir en bas de page](https://developer.mozilla.org/fr/docs/Web/CSS/filter) ou encore [CanIUse](https://caniuse.com)



# Aspect de la font

## font

[Propriété FONT](https://developer.mozilla.org/fr/docs/Web/CSS/filter)

 - `font-family` : Famille de la police de caractère (ou typo), `@font-face`
 - `font-size`: Taille (en EM, REM, PX, %)
 - `font-weight` : Graisse (bold, normal, 100 < 900)
 - `font-style` : italic | normal
 - `font-variant` : Petites capitales (Attention à la font)

## font sans font

 - `line-height` : Interlignage
 - `text-transform` : Capitale, UPPER, lower
 - `color` : #HEXA, rgb, rgba
 - `text-decoration` : underline, none, etc...


## Texte

- `white-space` : Gestion des blancs
- `letter-spacing` : Espacement des lettres
- `text-align` : left|right|center|justify
- `word-spacing` : Espacement des mots
- `letter-spacing` : Espacement des lettres
- `text-shadow` : Ombres/lumières


# Bloc

## Arrière plan

 - `background-color` : couleur
 - `background-repeat` : répétition
 - `background-position` : placement
 - `background-url` : url()
 - `background-size` : COVER / TAILLE

## Habillage

- `border` : Voir doc
- `padding` : espacement intérieur
- `margin` : espacement extérieur
- `border-radius` : Arrondis
- `box-shadow` : Ombre / lumière
- `opacity` : alpha
- `visibility` : hidden (on reviendra sur ça)
- `transform` : scale(X,Y), rotate(Ddeg), translate(X,Y), skew(Xdeg,Ydeg)
- `filter` : blur(PX), greyscale(%), contrast(%), saturate(%) etc...
