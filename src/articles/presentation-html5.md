# Technologies du web

La création de contenu pour le web est l'aggrégat d'un **ensemble de technologies** que l'usage profane regroupe généralement sous le terme **technologies HTML/Web**.

## HTML

Le HTML est une technologie utilisée pour :

 - Créer des documents web (pages HTML)
 - Structurer les interfaces web (Webapp)

Le HTML est un langage :

 - Description de contenu par **balise**
 - Conçu pour être facile à utiliser/partager
 - **Sémantique** (Il sert à structurer une informations - pas à la mise en forme)

<div class="info">
HTML n'est généralement pas considéré comme un **langage de programmation**. Il est en effet purement descriptif et ne propose aucun mécanisme logique.
</div>


## Petite histoire

### 1990

<div class="cols">
<div class="col">
Afin de répondre au besoin d'échange d'information au CERN, **Tim Berners** pose les bases du web :

- Premier navigateur,
- le protocole HTTP (pour l'accès aux document HTML),
- et le langage HTML

Il rends sa création publique, gratuite et ouverte. L'une des première page web était un tutoriel pour apprendre à créer des pages web...
</div>
<div class="col">
![Tim Berners Lee](../images/berners.jpg)
</div>
</div>



### 1993

<div class="cols">
<div class="col">
  ![Le navigateur NCSA Mosaic](../images/ncsa-mosaic.jpg)
</div>
<div class="col">
  Le **Web** s'étend à la communauté scientifique. Les universités sont *connectées* entre pour échanger les connaissances et les rendre accessible.

  Son usage se popularise et s'étends grâce à **NCSA Mosaic**, un navigateur capable d'afficher... des images.
</div>
</div>


### 1994-1996

<div class="cols">
<div class="col">
**Netscape navigator** popularise le web hors des universités en permettant de réaliser les premiers sites *visuels*.

 - Fondation du **W3C** (World Wide Web Consortium)
 - Apparition d'**Internet Explorer**
 - 1995/96 : Spécification de HTML 2.0 est publiée.
</div>
<div class="col">
  ![Netscape navigator](../images/disquette-netscape-navigator.jpg)
</div>
</div>

Cette période voit également s'affronter les éditeurs de navigateur Netscape et Explorer dans ce qu'on appelle **Le guerre des navigateurs**. Ces derniers n'hesitant pas à prendre de grandes liberté quand au rendu et aux fonctionnalités HTML/CSS


### 1997/2000 : Normalisation

<div class="cols">
<div class="col">
**HTML 3.2** Syntètise les améliorations apportées par netscape et Internet explorer, en vu de normaliser les usages des 2 navigateurs.

**HTML 4** sort dans la foulée. Arrivée en force des **style CSS** et des scripts (Javascript). Le W3C arrive à imposer ce standard

La production de contenu Web se professionnalise
</div>
<div class="col">
  ![W3C](../images/w3c.png)
</div>
</div>


### 2000/2008 : Web 2.0

Le **XHTML** s'impose dans l'usage professionnel, il est la version **strict** de HTML 4. Il scèle une bonne fois pour toute la sacro-sainte séparation du contenu (XHTML) et de la forme (CSS).

La production de site web se professionnalise, les métiers d'**intégrateur web** / **Webdesigner** sont reconnus comme des disciplines hautement technique et plus seulement comme des activités complémentaires à des domaines plus générale (Designers / Informaticiens).

<div class="info">
L'expression **Web 2.0** est un terme purement maketing, les technologies derrière cette pratique avaient déjà presque 10 ans.
</div>


### Maintenant

<div class="cols">
<div class="col">
Le **XHTML 2**, une nouvelle norme développée depuis plusieurs années a été définitivement abandonné ; rejetée par la communauté avant même d'être publié.

Le **HTML5** et le **CSS3**, encore en brouillon, sont déjà largement adoptés par les principaux navigateurs (Firefox, Chrome, Edge). Imposant définitivement l'aire du **Web Sémantique**.
</div>
<div class="col">
  ![Web sémantique](../images/semantic.svg)
</div>
</div>


## Le World Wide Web

La transmission et l'accès aux contenus du web est déterminé par 3 choses :

 - Le **navigateur web**
 - Le protocole HTTP/S
 - Le langage HTML

### Le navigateur

Le navigateur est un logiciel **client** qui va permettre de consulter des documents web. Nous reviendrons sur ce point, mais il faut retenir que le navigateur **interprète** le code source d'une page web avant de l'afficher, il peut donc arriver que le rendu d'un contenu varie entre 2 navigateurs.

![Répartition des navigateurs en 2018 - source : http://gs.statcounter.com/](../images/navigateurs.png)

Généralement, les intégrateurs web s'assure d'un fonctionnement idéale sur les principaux navigateurs dans les version récentes (1 à 2 ans), puis s'assure d'un fonctionnement optimal sur des versions plus anciennes et les navigateurs moins présents sur le marché.

### Le protocole HTTP

Tous les systèmes informatiques utilisent des **protocoles de communications** pour s'échanger des données. Le web utilise le **protocole HTTP** (et HTTPS dans sa version sécurisée).

Ce protocole est un protocole **client / serveur**. Le client c'est le navigateur, et le serveur sera une application serveur (généralement sur internet, mais pas que).

![Le protocole HTTP](../images/http.png)

### Le langage HTML

Voir cours suivant [Débuter en HTML](./premiere-page-web.html)


