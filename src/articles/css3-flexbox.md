# Comprendre les FLEXBOX

Les **flexbox** sont un dispositif de mise en page introduit avec le *CSS3* pour faciliter l'organisation des contenus. Ils remplacent avantageusement les mécanismes basés sur `float` utilisés en **CSS2**.

## préambule

Voice le code de base que nous allons faire évoluer tout au long de l'article :

```html
<!-- Code CSS -->
<div class="conteneur">
  <div class="element orange">A</div>
  <div class="element jaune">B</div>
  <div class="element cyan">C</div>
  <div class="element rouge">D</div>
  <div class="element vert">E</div>
</div>
```

```css
/**** MISE en PAGE ****/
.conteneur {
  /* Y'aura du code ici... */
}

.element {
  /* ... et là */
}

/**** MISE en FORME ****/
/* Cette partie sert uniquement à gérer l'apparence afin
de rendre le résultat plus 'visuel'  */
.conteneur {
  background: purple;
  padding: 1em;
}

.elem {
  font-size: 2em;  
}

.orange { background: orange; }
.jaune { background: yellow; }
.cyan { background: cyan; }
.rouge { background: red; }
.vert { background: green; }
```

Ce qui donne :
![Rendu de base](../images/css3-flexbox-rendu-001.png)

## Les bases

### flex et inline-flex

Le modèle de boîte doit s'activer **sur l'élément conteneur** (celui qui contient les éléments à mettre en page), en renseignant la valeur `display` avec les valeurs  `flex` ou `inline-flex` .

```css
/**** MISE en PAGE ****/
.conteneur {
  display: flex;
}
```

![Rendu avec la valeur flex](../images/css3-flexbox-rendu-002.png)

La valeur `inline-flex` ajoute un compotement **inline** au conteneur : 

```css
/**** MISE en PAGE ****/
.conteneur {
  display: inline-flex;
}
```

![Rendu avec la valeur flex](../images/css3-flexbox-rendu-003.png)

L'activation des *flexbox* se déclenche en utilisant les valeurs :

* `flex` : le conteneur se comporte comme un **block**
* `inline-flex` : le conteneur se comporte comme un élément **inline**

### Gérer la direction avec `flex-direction`

La propriété `flex-direction` permet de fixer le comportement général des éléments dans le conteneur. par défaut, les éléments s'affichent en ligne (valeur `row`).

```css
/**** MISE en PAGE ****/
.conteneur {
  display: inline-flex;
  flex-direction: row;
}
```
Les valeurs disponibles sont : 
 * `row` *(par défaut)* Le conteneur se comporte comme une ligne d'éléments alignés à gauche dans le sens de la lecture,
 * `row-reverse` Les éléments sont alignés à droite et sont inversés
 * `column` Le conteneur se comporte comme une colonne, les éléments sont affichés de haut en bas
 * `column-reverse` Idem, mais les éléments sont inversés

A noter que la valeur `column-reverse` justify les éléments en bas du conteneur. Vous pouvez le vérifier en appliquant arbitrairement une hauteur à ce dernier : `height: 250px`

```css
/**** MISE en PAGE ****/
.conteneur {
  display: flex;
  flex-direction: column-reverse;
  height: 250px;
}
```
![Rendu de column-reverse avec une hauteur fixée](../images/css3-flexbox-rendu-004.png)

### Réorganiser les éléments avec `order`

La propriété `order` permet de modifier l'ordre d'affichage d'un élément spécifique. Par défaut, cette valeur est fixée à 0, mais elle peut être modifié sur chaque éléments pour réorganiser l'enchainement des éléments dans le conteneur.

```css
.conteneur {
  display: flex;
}
.rouge {
    order: -1;
}
```
![Rendu de column-reverse avec une hauteur fixée](../images/css3-flexbox-rendu-005.png)

Vous pouvez ainsi complètement modifier la disposition des éléments dans le conteneur.

### Emballage des éléments avec `flex-wrap`

Dans notre exemple, les éléments ont assez de place pour tenir dans le conteneur car ils comportent peu de contenu. Mais il faut garder à l'esprit que la règle **le contenu l'emporte** s'applique également aux flexbox : 

Ajouter un peu de contenu dans les éléments HTML : 

```html
<div class="conteneur">
  <div class="element orange">ORANGE</div>
  <div class="element jaune">JAUNE</div>
  <div class="element cyan">CYAN</div>
  <div class="element rouge">ROUGE</div>
  <div class="element vert">VERT</div>
</div>
```

Puis réduisez la largeur de la fenêtre en largeur, vous verrez le contenu disparaître vers la droite : 

![Sans flex-wrap, le contenu dépasse sur la droite](../images/css3-flexbox-rendu-006.png)

La propriétés `flex-wrap` (*nowrap*, `wrap`, `wrap-reverse`) va forcer la *flexbox* à adapter l'embalage.

La valeur `wrap` va autoriser la flexbox à ajouter des lignes (ou des colonnes) pour garantir l'affichage du contenu : 

```css
.conteneur {
  display: flex;
  flex-wrap: wrap;
}
```
![Rendu de column-reverse avec une hauteur fixée](../images/css3-flexbox-rendu-007.png)

La valeur wrap-reverse inverse la disposition des lignes.

A noter que `flex-wrap` ne semble pas donner de résultat si `flex-direction` est réglé sur `column`. Mais il a bien un effet, pour l'observer, ajouter une hauteur au conteneur : 

```css
.conteneur {
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  height: 7em;
}
```

## Gérer la taille des éléments

La taille des éléments d'un flexbox s'ajuste avec 3 propriétés : 

* `flex-grow` : Agrandissement
* `flex-shrink` : Contraction
* `flex-basis` : Taille optimale
* `flex` : Forme compacte de ces 3 propriétés

### Élargir les éléments avec `flex-grow`

La propriété `flex-grow` permet, au sein d'un item,  de définir la façon dont la taille de l'élément peut être élargi. Par défaut, `flex-grow` est fixé à 0, donc les éléments ne s'élargissent, leurs taille maximum correspond à leur taille initiale.

En mettant la valeur 1 à tout les éléments, la *flexbox* va recalculer la largeur des éléments en équilibrant leur occupation (tous à la même taille) pour occuper toute la largeur disponible dans le conteneur : 

```css
.conteneur {
  display: flex;
}

.element {
  flex-grow: 1;
}
```
![Rendu de column-reverse avec une hauteur fixée](../images/css3-flexbox-rendu-008.png)

En jouant sur la largeur de la fenêtre, vous verrez la largeur des éléments s'adapter en fonction de l'espace disponible. Vous pouvez constater que la largeur est identique pour chaques éléments, peut importe son contenu.

Vous pouvez ensuite jouer avec la valeur de `flex-grow` (qui doit être un entier strictement positif) pour répartir la distribution de l'espace disponible différement en fonction de l'élément : 

```css
.conteneur {
  display: flex;
}

.element {
  flex-grow: 1;
}
.orange {
	/* orange sera 3 fois plus grand que les autres */
    flex-grow: 3;
}
.vert {
    /* Vert n'est pas autorisé à grandir */
    flex-grow: 0;
}
```

![Rendu de column-reverse avec une hauteur fixée](../images/css3-flexbox-rendu-009.png)

Pour observer le résultat avec un direction en colonne avec `flex-direction:column`, pensez à ajouter une hauteur au conteneur : 

```css
.conteneur {
  display: flex;
  flex-direction: column;
  height: 350px;
}

.element {
  flex-grow: 1;
}

.orange {
  flex-grow: 3;
}

.vert {
  flex-grow: 0;
}
```
![Rendu de column-reverse avec une hauteur fixée](../images/css3-flexbox-rendu-010.png)

### Taille initale avec flex-basis

Par défaut, la taille des éléments est établie en fonction du contenu. la propriété `flex-basis` permet de définir la taille initiale d'un élément avant que l'espace disponible soit redistribué entre les éléments.

Par exemple, nous pouvons indiquer comme largeur optimale 200px, et interdire le bloc orange de grandir en fixant `flex-grow` à 0 : 

```css
.conteneur {
  display: flex;
}

.element {
  flex-grow: 1;
}
.orange {
    flex-grow: 0;
    /* Taille initiale de 200 */
    flex-basis: 200px;
}
```
![Rendu de column-reverse avec une hauteur fixée](../images/css3-flexbox-rendu-011.png)

En jouant sur la largeur de la fenêtre, vous constaterez que la zone orange peut rétrécir, mais ne dépasse pas les 200 pixels en largeur lorsque l'on élargi la fenêtre.

### Contraction avec `flex-shrink`

La propriétés `flex-shrink`permet quand à elle de configurer le rétrécissement des éléments (C'est la même mécanique de `flex-grow`, mais ).

```css
.conteneur {
  display: flex;
}
.element {
  flex-grow: 1;
}
/* Orange ne peut pas rétécir ou s'agrandir. 
	Sa taille est de 200px */
.orange {
	flex-shrink: 0;
    flex-grow: 0;
    flex-basis: 200px;
}
```

![Rendu de column-reverse avec une hauteur fixée](../images/css3-flexbox-rendu-012.png)

### Forme compacte avec `flex`

TODO

## Alignement et positionnement des éléments

### Justification et alignement

#### `Alignement dans l'axe principal : justify-content`

La propriété `justify-content` permet de définir la façon ton les éléments vont s'aligner le long de l'axe principal (Axe horizontal). 

Petit récap des valeurs possibles : 

![Rendu de column-reverse avec une hauteur fixée](../images/css3-flexbox-rendu-013.png)

##### flex-start

Les éléments sont alignés au début de la *flexbox* ; à gauche ou en haut selon la valeur de `flex-direction`. C'est le réglage par défaut.

```css
.conteneur {
  display: flex;
  justify-content: flex-start;
}
```

##### flex-end

Les éléments sont alignés à la fin de la *flexbox* ; à droite ou en bas selon la valeur de `flex-direction`

```css
.conteneur {
  display: flex;
  justify-content: flex-end;
}
```
##### center

Les éléments sont alignés à la fin de la *flexbox* ; à droite ou en bas selon la valeur de `flex-direction`

```css
.conteneur {
  display: flex;
  justify-content: center;
}
```