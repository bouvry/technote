# CSS : Le modèle de boîte

Une des clefs pour maîtriser la mise en page avec CSS, c'est de bien comprendre le **modèle de boîte**.

## BLOCK et INLINE

Il y'a 2 modes d'affichage à bien comprendre :

 - `block` elles vont rompre le flux
 - `inline` elles s'ajoutent dans la continuté du flux 
