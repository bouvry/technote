# Débuter en HTML (Votre première page web)

## Page web, document HTML

### Document HTML

Le code HTML s'écrit dans des **fichiers HTML** ; Les fichiers HTML sont de simples fichiers texte.

N'importe quel éditeur de texte suffit pour éditer du HTML.

Par convention, on utilise l'extention `*.html` (ou `*.htm` pour les puristes).

Pour tester le résultat, il suffit de l'ouvrir dans un navigateur.

### Rédiger du code HTML

Pour faire du HTML, il faut un éditeur de texte, pour débuter voici quelques éditeurs très performants :

 - **Sublim Text** (Presque payant) Un des plus populaire chez les développeurs web
 - **Notepad++** Très utilisé sous Windows
 - **Bracket** Très pratique pour débuter (coloration du code, aperçu en directe) http://brackets.io/
 - **Atom** Un éditeur léger et efficace https://atom.io/

### Premier contenu

Créez un fichier `index.html` dans l'éditeur avec ce contenu :

```
Bonjour Monde !
```

Utilisez le menu Fichier/ouvrir du navigateur pour afficher le fichier :

![Résulat](../images/html-bonjour-monde.png)

<div class="info">Même rudimentaire, nous avons une page web</div>



## Syntaxe HTML


### Balise

Le **code source** HTML permet de structurer les informations avec des **balises**.

Les balises sont **toujours entourées de chevrons**

Dans 99% des cas, un nom de balise est écrit en minuscule et ne contient pas de caractères accentués, ni espaces.

![Résulat](../images/balise.svg)


### Types de balises

On distingues 3 différents types de balises :

 - Les balises ouvrantes, ex: `<h1>`, `<p>`,
 - Les balises fermantes, ex: `</h1>`, `</p>`,
 - Les balises orphelines (ou auto-fermantes)ex: `<br />`,

### Balises ouvrantes et fermantes

Les balises **ouvrantes** et **fermantes** permettent de délimiter du contenu. C'est le cas le plus répandu :

```html
<h1>Ce contenu sera un titre</h1>
<p>
    Ce contenu sera un paragraphe,
    avec
    <strong>
        ce texte en gras
    </strong>
</p>
```

![Résultat](../images/balises-ouvrantes-fermantes.svg)


### Balises auto-fermantes

Certaines balises ne délimitent aucune information, elles sont utilisées pour indiquer la présence d'un contenu spécifique (présence d'une image par exemple).

Plutôt que de les ouvrir et de le refermer, sans qu'elles ne délimitent aucun contenu ex :`<balise></balise>`, on utilise une syntaxe raccourci : `<balise />`

```html
<!-- Liseré horizontal -->
<hr />

Texte avec un retour chariot<br />
Ce texte est à la ligne
```

![Balises autofermantes](../images/balise-auto-fermante.svg)

### Récapitulatif

![Récapitulatif](../images/balise-recap.svg)



### Imbrication

Nous venons de voir que les balises sont utilisées pour **délimiter du contenu**, dans les exemples précédents, le contenu été uniquement du contenu texte, mais généralement, le contenu est souvent un aggrégat de texte contenant lui-même des balises, pouvant à son tour contenir des balises. (à la manière de poupées russes). Ce formalisme est appelé **l'imbrication**.

```html
<body>
  <h1>Bonjour monde !</h1>
  <p>
    Ceci est ma première page<br/>
    elle contient du
    <strong>HTML</strong> !
  </p>
</body>
```

L'imbrication respecte la règle : **Première ouverte, dernière fermée**. Less balises qui *se croisent* ou qui ne sont pas refermée peuvent provoquer des erreurs d'interprétation.

### Arbre DOM

De part sa nature, l'imbrication peut être représenté sous la forme d'un arbre, on parle de l'arbre DOM (Document Object Model).

![L'arbre DOM](../images/html-imbrication.png)

### Commentaires

Un code source HTML peut devenir rapidement dense et atteindre plusieurs centaines de ligne(on écrit beaucoup de code).

Les **commentaires** sont très utilisés pour permettre aux intégrateurs d'annoter le code source sans que ce commentaire apparaisse dans le rendu final.

Un commentaire débute par la séquence `<!--` et se termine avec `-->`

```html
<body>
  <!-- Titre de la page -->
  <header>
    <h1>Bonjour monde !</h1>
  </header>

  <!-- Menu principale -->
  <nav>
    <a href="#">Accueil</a>
    <a href="gallery.html">Galerie</a>
  </nav>

  <!-- Contenu -->
  <p>Contenu de la page</p>

  <!-- Pied de page -->
  <footer>
    Credit © Unicaen.fr
  </footer>
</body>
```

L'utilisation des commentaires est également pratique pour désactiver certaines parties du code.

```html
<body>
  <!-- Titre de la page -->
  <header>
    <h1>Bonjour monde !</h1>
  </header>

  <!-- Menu principale (désactivé) -->
  <!--
  <nav>
    <a href="#">Accueil</a>
    <a href="gallery.html">Galerie</a>
  </nav>
  -->

  <!-- Contenu -->
  <p>Contenu de la page</p>

  <!-- Pied de page -->
  <footer>
    Credit © Unicaen.fr
  </footer>
</body>
```

### Attributs de balise

Les attributs de balise permettent d'indiquer des informations au navigateur.
                    
                                       
Exemple : La balise `<img />` est utilisée pour afficher 
des image, Seule elle n'a aucun interêt. L'attribut `src="URL"` 
indique l'emplacement de l'image : 

```html
<img src="http://goo.gl/GzaoyV" />
```

<div class="info">  
Les attributes n'apparaissent que dans les
balises ouvrantes et les
balises auto-fermantes.
</div>

### Syntaxe des attributs

La syntaxe des attributs est toujours la même : 

```html
<balise attribut1="Valeur entre guillemets">
```


On peut cumuler les attributs en les séparant avec au moins un espace : 

```html
<img alt="Illustration Hi-Tech" src="http://goo.gl/GzaoyV" />
```

On peut également utiliser les retours à la ligne pour gagner en clarté : 

```html
<img alt="Illustration Hi-Tech" 
    src="http://goo.gl/GzaoyV" 
    width="1280"
    height="760"
 />
```

## Document HTML standard

Les documents HTML sont standardisés pour permettre au navigateur de correctement interpréter le code.

### Structure d'un document HTML (minimale)

Voici la structure d'un document HTML

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Titre de la page</title>
    </head>
    <body>
        Corps de la page
    </body>
</html>
```

 - La balise `<html>` encadre le document (c'est le noeud racine)
 - Le `<head>` va contenir des métas informations à l'attention du navigateur
 - Le `<body>` contiend la partie *visible* de la page

<div class="info">
Le prologue `<!DOCTYPE html>` n'est pas une balise, il indique au navigateur la norme du document (ici HTML5).
</div>

### Validation

Vous pouvez tester la validité d'un code HTML en utilisant le [Validateur W3C](http://validator.w3.org/)

![Validateur W3C](../images/validator.jpg)
