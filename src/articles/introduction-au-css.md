# Les feuilles de style en cascade : CSS3

## Présentation

### CSS, c'est quoi ?

Le CSS est un langage dédié à la mise en forme de document web. Il permet de **séparer le contenu et la mise en forme**. C'est un langage *déclaratif* dont les régles de syntaxe s'apprennent très vite, cependant, il nécessite une pratique assidue pour être parfaitement maîtrisé.


### Un langage interprété

Tout comme le HTML auquel il est fortement lié, le CSS est un **langage interprété**, c'est le **navigateur qui assure le rendu** et selon le système et/ou le navigateur, le résultat peu varier. Si le navigateur de *comprend pas* une régle ou une propriété CSS, il l'ignorera.

<div class="info">
Le site [Can I Use](https://caniuse.com/) propose une liste exhaustive des technologies prises en charge en CSS/javascript pour s'assurer de la disponiblité de la technologie selon le navigateur et la version utilisée.

Cependant, les intégrateurs web s'autorisent un seuil de tolérance pour le cas (rare) des navigateurs anciens. Il applique la règle de la **dégradation disgracieuse** consistant à valider uniquement l'affichage acceptable des informations en fixant des limite de version.
</div>


### Compatibilité CSS2 /CSS3

Le **CSS2** et le **CSS3** sont parfaitement compatibles, en effet, le CSS3 se contente d'introduire uniquement de nouvelles propriétés dédiées à la mise en forme et quelques sélecteurs.
<!--  ![Logo CSS](../images/css3.svg) -->


## Syntax CSS

le langage CSS est uniquement composé de **régle CSS** qui vont indiquer au navigateur quelle mise en forme doit être appliquée à quel élément.

Ces régles se composent :

 - D'un **sélecteur** qui va dire à quel(s) élément(s) appliquer la mise en forme
 - Une ou plusieurs **déclarations** qui vont préciser comment mettre en forme

Une régle dira :

```plain
Pour les éléments ARTICLE (sélecteur):
  Mettre le texte en blanc (déclaration)
  Mettre le texte en justifié (déclaration)
  Mettre la taille de la police à 18 pixel (déclaration)
  Mettre le fond en noir (déclaration)
```  

### Syntaxe des règles

Voici un exemple de règle CSS correspondant à celle rédigé littéralement

```css
article {
  color: #FFFFFF;
  text-align: justify;
  font-size: 18px;
  background: #000000;
}
```  

### Le sélecteur

<div class="cols">
<div class="col">
  le sélécteur va permettre de *sélectionner* un ou plusieurs éléments sur lesquels vont s'appliquer la régle.
</div>
<div class="col">
  ![](../images/regle-css-02.png)
</div>
</div>


### Le bloc de déclaration

<div class="cols">
<div class="col">
  Une régle peut contenir plusieurs déclarations, elles sont regrouper dans le **bloc de déclaration** qu'on peut facilement identifié car il est entouré d'accolade :
</div>
<div class="col">
  ![](../images/regle-css-03.png)
</div>
</div>

### Déclaration

<div class="cols">
<div class="col">
  Une déclaration décrit une mise en forme à appliquer. Les déclarations sont séparées par des point-virgules. Par convention, on place une déclaration par ligne et l'on met un point-virgule à la dernière même si elle est facultative.
</div>
<div class="col">
  ![](../images/regle-css-04.png)
</div>
</div>

### Propriétés : Valeur

<div class="cols">
<div class="col">
  Une déclaration se compose d'une **propriété** et d'une **valeur** séparées par **deux point**. Chaque déclaration se termine par **un point-virgule**. Une déclaration est toujours sous la forme `propriété: valeurs`
</div>
<div class="col">
  ![](../images/regle-css-05.png)
</div>
</div>



## Intégration CSS

Le code CSS est complémentaire au HTML. Pour qu'il soit pris en compte dans une page web, il faut **intégrer le CSS** au document HTML.

Il existe 3 méthodes :

 - Le CSS en ligne avec l'attribut `style=""`
 - Le CSS de document avec la balise `<style></style>`
 - Le CSS attaché avec `<link href="" />`

### Style en ligne

Les styles en lignes permettent d'écrire les régles CSS **directement dans l'élément HTML** en utilisant l'attribut `style` :

```html
<p style="color: green">
  Texte du paragraphe en vert.
</p>
```

<div class="error">
Très mauvaise pratique !
</div>

### Style de document
La balise `style` permet de déclarer du style directement dans un document.

```html
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8" />
    <title>Exemple de style en ligne</title>

    <!-- Style de docuement -->
    <style type="text/css">
        p {
            color: white;
            background: black;
        }
    </style>
  </head>
  <body>
    <h1>Les style CSS</h1>
    <p>Mon premier paragraphe</p>
    <p>Un autre paragraphe</p>
  </body>
</html>
```

Cette utilisation est peu usité car les styles ne s'appliquent qu'au document courant.

### Feuille de style

Pratique la plus courante et la **plus recommandée**.

On commence par rédiger le CSS dans un fichier `*.css` séparé :

```css
/** Fichier feuille-de-style.css **/
p {
  color: white;
  background: black;
}
```

Puis on **attache** la feuille de style au document HTML en utilisant la balise `link` :

```html
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8" />
    <title>Exemple de style en ligne</title>
    <!-- On attache la feuille de style -->
    <link href="ma-feuille-de-style.css" rel="stylesheet" type="text/css" />

  </head>
  <body>
    <h1>Les style CSS</h1>
    <p>Mon premier paragraphe</p>
    <p>Un autre paragraphe</p>
  </body>
</html>
```

Cette usage permet de réutiliser la même feuille de style dans plusieurs documents sans surcoût réseaux. Il facilite également la maintenance visuel d'un site en centralisant les régles de mise en forme dans un unique fichier.


## Les sélecteurs CSS

Dans une **régle CSS**, le sélecteur permet au navigateur de déterminer à quels éléments HTML les déclarations s'appliquent.

Les **sélecteurs CSS** sont très variés, le principe reste simple, mais leur mise en oeuvre peut vite devenir compliquée si l'on manque de soin et de rigueur.

Voici quelques sélecteurs courants :

### Le sélecteur global

Un sélecteur d'une *grande subtilité*, car il sélectionne tous les éléments :

```css
* {
  color: #ff6600;
}
```

Cela équivaut à faire sélectionner toutes les balises une par une.

#### Le piège

Malgrès son caractère séduisant pour un débutant, ce sélecteur peut vite vous faire tourner en bourique :

```css
* {
  /** texte souligné **/
  text-decoration: underline;
}

header {
  /* Le texte de la balise HEADER
     n'est pas souligné. */
  text-decoration: none;
}
```

```html
<header>
    <h1>Souligné :(</h1>
    <p>Souligné aussi</p>
</header>
```

Dans cet exemple, on pourrait s'imaginer que le fait de supprimer le texte souligné du `header` s'appliquerait également aux éléments qui le compose (`h1` et `p`). mais l'utilisation du sélecteur globale équivaut à écrire cette règle :

```css
header, p, h1 /* et toutes les autres balises */ {
  text-decoration: underline;  
}
```

### Le sélecteur d'élément

Permet de sélectionner un élément via le **nom de balise** :

```css
/* Les paragraphes */
p {
  font-size: 16px;
  color: #444444;
  text-align: justify;
}

/* Les gros titres */
h1 {
  font-size: 48px;
  font-weight: normal;
  color: #7700bb;
}
```

### Le sélecteur d'identifiant

Permet de sélectionner un élément via sont **Identifiant unique**.

On commence par utiliser l'attribut HTML `id=""` pour identifier un élément dans la page :

```html
<header id="bandeau-du-site">
    <h1>Mon site</h1>
    <p>Un site bien classe (CSS)</p>
</header>
```

Puis on utilise le sélecteur d'identifiant (Oui, on peut l'appeler le sélecteur *hashtag* si ça vous fait plaisir), Il reprends l'identifiant précédé par un `#` :

```css
#bandeau-du-site {
    background-image: url('http://goo.gl/GzaoyV');
    color: #000000;
}
```

### Le sélecteur de classe

Ce sélecteur permet de définir des **classes CSS** puis de les appliquer à n'importe quel éléments du HTML.

On commence par **déclarer une classe CSS** :

```css
/* texte barré, rouge */
.erreur {
    text-decoration: line-through;
    color: #990000;
}
```

Puis dans le HTML, on utilise l'attribut `class=""` pour appliquer cette classe à l'élément :

```html
<article>
  <h2>Titre</h2>
  <p class="erreur">Ce texte est érroné</p>
</article>
```

#### Plusieurs classes, un élément

Ce système permet également d'**appliquer plusieurs classe CSS à un même élément** HTML.

```css
.texte-barre {
  text-decoration: line-through;
}
.texte-rouge {
  color: #990000;  
}
```

Puis en renseigne l'attribut `class=""` avec les classes CSS séparés par un espace :

```html
<article>
  <h2>Titre</h2>
  <p class="texte-barre texte-rouge">Ce texte est en rouge et barré</p>
</article>
```

#### Plusieurs classes, un élément (oui c'est le même titre)

Autre cas plus subtile, on peut écrire un sélécteur qui va s'appliquer si un élément dispose de 2 classes. Commençons par créer 2 classes qui combinées vont poser problème :

```css
.texte-rouge {
  color: red;
}
.fond-rouge {
  background: red;  
}
```

```html
<article>
  <p class="texte-rouge">texte en rouge</p>
  <p class="fond-rouge">fond en rouge</p>
  <p class="texte-rouge fond-rouge">texte en rouge</p>
</article>
```

Dans cet exemple, le dernier paragraphe a un problème car le texte et le fond sont rouge. On va donc ajouter une régle CSS pour ajouter un cas particulier pour les éléments qui ont les 2 classes :

```css
.texte-rouge.fond-rouge {
  color: #330000;  
}
```
Notez qu'il n'y a pas d'espace entre `.texte-rouge` et `.fond-rouge`.

### Regroupement de sélecteur

Si plusieurs éléments doivent avoir une même régle, on peut les cumuler dans une même déclaration en les séparant par une virgule (le petit espace est là juste pour des raisons de lisibilité):

```css
h1, h2, h3, h4, h5, h6, strong {
  color: #ff6600; /* Orange */
}
```

Dans cet exemple, le regroupement utilise des sélecteurs d'éléments, mais rien n'empèche d'utiliser les autres sélecteurs vu précédement :

```css
.texte-important, h1, strong, #titre-page, .texte-leger.fond-kaki {
    font-weight: bold;
}
```

### Sélecteur d'imbrication

Le selecteur d'imbrication permet d'appliquer une règle lorsque les éléments sont agencés d'une certaines façon :

```css
/* les titres H1 sont en vert */
h1 {
  color: green;
}

/* Par contre, les titres H1 dans un ARTICLE sont en jaune */
article h1 {
  color: yellow;
}
```

```html
<section>
  <h1>Mes articles (en vert)</h1>
  <article>
    <h1>Article 1 (en jaune)</h1>
  </article>
</section>  
```

La aussi, ont peu combiner tous ça avec un regroupement de sélecteur :

```css
/* s'applique aux articles ayant la classe "cool" */
article.cool {
    font-weight: bold;
}

/* s'applique aux en-tête-header) des sections ayant la classe "nouveau" */
section.nouveau header {
    background-image: url('badge.jpg');
}

/* s'applique aux en-tête des articles "cool",
   au H1 du header ayant l'id "bandeau" */
article.cool header, header#bandeau h1 {
    background-image: url('badge.jpg');
}
```

Comme on peut le voir, les sélecteurs commence à se complexifier progressivement.

Nous approfondirons le sujet par la suite.
