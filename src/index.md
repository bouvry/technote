# Note de développement

## Intégration web


### Bases

 - [Présentation des technologies du web](articles/presentation-html5.html)
 - [Première page web](articles/premiere-page-web.html)
 - [Les bases du CSS](slides/introduction-au-css.html)
 - [Structures HTML](slides/structure-html.html)
 - [Propriétés CSS](slides/proprietes-css.html)
 - [Gestion de site](slides/gestion-de-site.html)

###  
<!--
 - [Structurer du contenu avec HTML](articles/structure-html.html)
 - [Mise en forme CSS](articles/mise-en-forme-css.html)
 - [Modèle de boîte](slides/modele-de-boite.html)
-->
<!--
###
 - [Mise en page avec FLOAT](articles/mise-en-page-float.html)
 - [Mise en page avec les **Flexbox**](articles/css3-flexbox.html)
 - [Design réactif : Responsive Design](articles/responsive-design.html)
-->
